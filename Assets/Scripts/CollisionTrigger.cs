﻿using UnityEngine;
using System.Collections;

public class CollisionTrigger : MonoBehaviour {

	private EdgeCollider2D playerCollider;

    [SerializeField]
	public BoxCollider2D platformCollider;

    [SerializeField]
	public BoxCollider2D platformTrigger;

	// Use this for initialization
	void Start () {

		playerCollider = GameObject.Find("Player").GetComponent<EdgeCollider2D>();
        platformCollider.enabled = false;
        //Physics2D.IgnoreCollision(platformCollider, platformTrigger, true);

    }

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "Player")
		{
            platformCollider.enabled = true;
        }
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.name == "Player")
		{
            platformCollider.enabled = false;
        }
	}

}