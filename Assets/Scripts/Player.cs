﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private static Player instance;

    public static Player Instance {
		
        get {
			
            if (instance == null) {
				
                instance = GameObject.FindObjectOfType<Player>();
            }
            return instance;
        }
    }

    private Animator myAnimator;

	[SerializeField]
	private float movementSpeed;

	private bool facingRight;
	
	public Transform groundStart, groundEnd, wallStart, wallEnd;

	[SerializeField]
	private LayerMask whatIsGround;

	[SerializeField]
	private LayerMask whatIsWall;

	private bool isWalled;

	[SerializeField]
	private bool airControl;

	[SerializeField]
	private float jumpForce;

	[SerializeField]
	private float maxSpeed;

    [SerializeField]
    public GameObject shot;

    public Transform shotSpawn;

	public Transform shotTarget;

    public float magicSpeed;

    public float fireRate;

    private float nextFire;

    public Rigidbody2D MyRigidbody { get; set; }

    public bool Attack { get; set; }

    public bool Jump{ get; set; }

    public bool OnGround { get; set; }

	[SerializeField]
	public bool fire = true;

	[SerializeField]
	public bool water = false;

	[SerializeField]
	public bool air = false;

	[SerializeField]
	public bool earth = false;

    // Use this for initialization
    void Start () {
		
		facingRight = true;

		MyRigidbody = GetComponent<Rigidbody2D> ();

		myAnimator = GetComponent<Animator> ();
	}
	
	void Update() {
		
		HandleInput();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		float horizontal = Input.GetAxis ("Horizontal");

		OnGround = IsGrounded();

		IsWalled ();

		Flip (horizontal);

		HandleMovement(horizontal);

		HandleLayers ();

        HandleMagic();
	}

	private void HandleMovement(float horizontal) {
		
		if (MyRigidbody.velocity.y < 0) {
			
			myAnimator.SetBool ("fall", true);
		} else {
			
			myAnimator.SetBool ("fall", false);
		}

		if ((OnGround || airControl) /*&& !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack")*/) {
		
            MyRigidbody.velocity = new Vector2(horizontal * movementSpeed, MyRigidbody.velocity.y); //x=-1, y=0;
        }

        myAnimator.SetFloat("speed", Mathf.Abs(horizontal));
    }


    private void HandleMagic() {
	
		if ((Input.GetKeyDown(KeyCode.Space)) && Time.time > nextFire /*&& !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !this.myAnimator.GetCurrentAnimatorStateInfo(1).IsTag("Attack")*/) {
            nextFire = Time.time + fireRate;

			Vector2 myPos = shotSpawn.transform.position;

			Vector2 target = shotTarget.transform.position;

			Vector2 direction = target - myPos;

			direction.Normalize();

			if (facingRight) {
				GameObject projectile = (GameObject)Instantiate (shot, myPos, Quaternion.Euler (0, 0, 0));

				Rigidbody2D magicRigidbody = projectile.GetComponent<Rigidbody2D>();

				magicRigidbody.velocity = direction * magicSpeed;
			}
			else {
				GameObject projectile = (GameObject)Instantiate (shot, myPos, Quaternion.Euler (0, 0, 180));

				Rigidbody2D magicRigidbody = projectile.GetComponent<Rigidbody2D>();

				magicRigidbody.velocity = direction * magicSpeed;
			}
        }
    }

    private void HandleInput() {
	
		if(Input.GetKeyDown(KeyCode.Space)) {
		
            myAnimator.SetTrigger("attack");
		}

		if(Input.GetKeyDown(KeyCode.UpArrow) && MyRigidbody.velocity.y == 0) {
		
            myAnimator.SetTrigger("jump");

			MyRigidbody.AddForce(new Vector2(0, jumpForce));
        }
			
		if (Input.GetKeyDown(KeyCode.Q)) {

			fire = true;

			water = false;

			air = false;

			earth = false;
		}

		if (Input.GetKeyDown(KeyCode.W)) {

			fire = false;

			water = true;

			air = false;

			earth = false;
		}

		if (Input.GetKeyDown(KeyCode.E)) {

			fire = false;

			water = false;

			air = true;

			earth = false;
		}

		if (Input.GetKeyDown(KeyCode.R)) {

			fire = false;

			water = false;

			air = false;

			earth = true;
		}
			
	}

	private void Flip(float horizontal) {
	
        if ((horizontal > 0 && !facingRight || horizontal < 0 && facingRight)/* && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack") && !this.myAnimator.GetCurrentAnimatorStateInfo(1).IsTag("Attack")*/) {
		
			facingRight = !facingRight;

			Vector3 theScale = transform.localScale;

			theScale.x *= -1;

			transform.localScale = theScale;
		}
    }

    private bool IsGrounded() {
	
		if (MyRigidbody.velocity.y == 0) {
		
			if (Physics2D.Linecast (groundStart.position, groundEnd.position, whatIsGround) == true) {
				
				return true;
			}
			else return false;
        }
        else return false;
	}

	private bool IsWalled() {
	
		if (MyRigidbody.velocity.y != 0) {
		
			isWalled = Physics2D.Linecast (wallStart.position, wallEnd.position, whatIsWall);
		}
		return isWalled;
	}

	private void HandleLayers() {
	
		if (fire) {

			myAnimator.SetLayerWeight(0, 1);
		}
		else {

			myAnimator.SetLayerWeight(0, 0);
		}

		if (water) {

			myAnimator.SetLayerWeight(1, 1);
		}
		else {

			myAnimator.SetLayerWeight(1, 0);
		}

		if (air) {

			myAnimator.SetLayerWeight(2, 1);
		}
		else {

			myAnimator.SetLayerWeight(2, 0);
		}

		if (earth) {

			myAnimator.SetLayerWeight(3, 1);
		}
		else {

			myAnimator.SetLayerWeight(3, 0);
		}
	}
	
}