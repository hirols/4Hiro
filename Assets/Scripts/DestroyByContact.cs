﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
	public GameObject explosion;

    private float delay = 1.0f;

	void Start ()
	{

	}
	
	void OnCollisionEnter2D()
	{

        var bombPos = transform.position;

        var cloneExplosion= Instantiate(explosion, bombPos, transform.rotation);

        Destroy(gameObject);

        Destroy(cloneExplosion, delay);
    }
}